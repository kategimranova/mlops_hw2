**Run tests** (*via pytest*)
```bash
pytest -v -p no:warnings
```